$('#form').submit(function(event) {
	$.post('form_submit.php', { email: $('[name=email]').val() }, function(data) {
		data = JSON.parse(data);

		if (data.error) {
			$('.alert').removeClass('alert-success')
				.addClass('alert-danger')
				.html('Enter a valid email address')
				.show();
		}
		else {
			$('.alert').removeClass('alert-danger')
				.addClass('alert-success')
				.html(data.email)
				.show();
		}
	});

	event.preventDefault();
});